class DiffusionModel:
    """Class defining a diffusion model"""

    def __init__(self, grid, phi, gamma, west_bc, east_bc):
        """Constructor"""
        self._grid = grid
        self._phi = phi
        self._gamma = gamma
        self._west_bc = west_bc
        self._east_bc = east_bc

    def add(self, coeffs):
        """Function to add diffusion terms to coefficient arrays"""

        # Calculate the west and east face diffusion flux terms for each face
        flux_w = - self._gamma*self._grid.Aw*(self._phi[1:-1]-self._phi[0:-2])/self._grid.dx_WP
        flux_e = - self._gamma*self._grid.Ae*(self._phi[2:]-self._phi[1:-1])/self._grid.dx_PE

        # Calculate the linearization coefficients
        coeffW = - self._gamma*self._grid.Aw/self._grid.dx_WP
        coeffE = - self._gamma*self._grid.Ae/self._grid.dx_PE
        coeffP = - coeffW - coeffE

        # Modify the linearization coefficients on the boundaries
        coeffP[0] += coeffW[0]*self._west_bc.coeff()
        coeffP[-1] += coeffE[-1]*self._east_bc.coeff()

        # Zero the boundary coefficients that are not used
        coeffW[0] = 0.0
        coeffE[-1] = 0.0

        # Calculate the net flux from each cell
        flux = flux_e - flux_w

        # Add to coefficient arrays
        coeffs.accumulate_aP(coeffP)
        coeffs.accumulate_aW(coeffW)
        coeffs.accumulate_aE(coeffE)
        coeffs.accumulate_rP(flux)

        # Return the modified coefficient array
        return coeffs

class SurfaceConvectionModel:
    """Class defining a surface convection model"""

    def __init__(self, grid, T, ho, To):
        """Constructor"""
        self._grid = grid
        self._T = T
        self._ho = ho
        self._To = To

    def add(self, coeffs):
        """Function to add surface convection terms to coefficient arrays"""

        # Calculate the source term
        source = self._ho*self._grid.Ao*(self._T[1:-1] - self._To)

        # Calculate the linearization coefficients
        coeffP = self._ho*self._grid.Ao

        # Add to coefficient arrays
        coeffs.accumulate_aP(coeffP)
        coeffs.accumulate_rP(source)

        return coeffs

class FirstOrderTransientModel:
    """Class defining a first order implicit transient model"""

    def __init__(self, grid, T, Told, rho, cp, dt):
        """Constructor"""
        self._grid = grid
        self._T = T
        self._Told = Told
        self._rho = rho
        self._cp = cp
        self._dt = dt

    def add(self, coeffs):
        """Function to add transient term to coefficient arrays"""

        # Calculate the transient term
        transient_term = self._rho*self._cp*self._grid._vol*(self._T[1:-1] - self._Told[1:-1]) * (1/self._dt)
        
        # Calculate the linearization coefficient
        coeffP = self._rho*self._cp*self._grid._vol*(1/self._dt)
        
        # Add to coefficient arrays
        coeffs.accumulate_aP(coeffP)
        coeffs.accumulate_rP(transient_term)

        return coeffs


class CrankNicholsonTransientModel:
    """Class defining a first order implicit transient model"""

    def __init__(self, grid, T, Told, Tolder, rho, cp, dt, k):
        """Constructor"""
        self._grid = grid
        self._T = T
        self._Told = Told
        self._Tolder = Tolder
        self._rho = rho
        self._cp = cp
        self._dt = dt
        self._k = k

    def add(self, coeffs):
        """Function to add transient term to coefficient arrays"""

        # Calculate the transient term
        transient_term = 2*self._rho*self._cp*self._grid._vol*(self._T[1:-1] - self._Told[1:-1]) * (1/self._dt)
        
        # Calculate previous diffusive fluxes
        Fw = -self._k*self._grid.Aw*(1/self._grid.dx_WP)*(self._Told[1:-1] - self._Told[0:-2])
        Fe = -self._k*self._grid.Ae*(1/self._grid.dx_PE)*(self._Told[2:] - self._Told[1:-1])
        flux = Fe - Fw
        
        # Calculate the linearization coefficient
        coeffP = 2*self._rho*self._cp*self._grid._vol*(1/self._dt) 
        
        # Add to coefficient arrays
        coeffs.accumulate_aP(coeffP)
        coeffs.accumulate_rP(transient_term)
        coeffs.accumulate_rP(flux)

        return coeffs

class SecondOrderTransientModel:
    """Class defining a second order implicit transient model"""

    def __init__(self, grid, T, Told, Tolder, rho, cp, dt):
        """Constructor"""
        self._grid = grid
        self._T = T
        self._Told = Told
        self._Tolder = Tolder
        self._rho = rho
        self._cp = cp
        self._dt = dt

    def add(self, coeffs):
        """Function to add transient term to coefficient arrays"""

        # Calculate the transient term
        transient_term = self._rho * self._cp * self._grid.vol * (
            1.5 * self._T[1:-1] - 2 * self._Told[1:-1] + 0.50 * self._Tolder[1:-1]) * (1/self._dt)
        
        # Calculate the linearization coefficient
        coeffP = 1.5*self._rho*self._cp*self._grid.vol*(1/self._dt)
        
        # Add to coefficient arrays
        coeffs.accumulate_aP(coeffP)
        coeffs.accumulate_rP(transient_term)

        return coeffs
